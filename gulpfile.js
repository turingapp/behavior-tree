var gulp = require('gulp');
var ts = require('gulp-typescript');
var clean = require('gulp-clean');
var mocha = require('gulp-mocha');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');

gulp.task('clean', function() {
    return del(['./release/']);
});

gulp.task('build', function() {
    return gulp
            .src(['./src/**/*.ts'])
            .pipe(sourcemaps.init())
            .pipe(ts('./tsconfig.json'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('./release'));
});

gulp.task('test', ['build'], function() {
    return gulp
            .src(['./test/**/*.ts'], {base: '.'})
            .pipe(sourcemaps.init())
            .pipe(ts('./tsconfig.json',{declaration: false}))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('./'))
            .pipe(mocha({reporter: 'spec'}));
});

gulp.task('watch', ['test'], function() {
    gulp.watch(['src/**/*.ts','./test/**/*.ts', '**/!(*.d.ts)'], ['test']);
});