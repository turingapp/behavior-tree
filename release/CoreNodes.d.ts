import { BaseNode } from './BaseNode';
export declare class Action extends BaseNode {
    constructor(id: string, name: string, title: string, description: string, properties: any);
}
export declare class Composite extends BaseNode {
    private _children;
    constructor(id: string, name: string, title: string, description: string, properties: any);
    readonly children: BaseNode[];
}
export declare class Decorator extends BaseNode {
    private _child;
    constructor(id: string, name: string, title: string, description: string, properties: any);
    child: BaseNode;
}
export declare class Condition extends BaseNode {
    constructor(id: string, name: string, title: string, description: string, properties: any);
}
