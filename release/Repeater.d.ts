import { Decorator } from './CoreNodes';
import { Tick } from './Tick';
import { TickResult } from './enums';
export declare class Repeater extends Decorator {
    private _maxLoop;
    constructor(id: string, title: string, description: string, properties: any);
    open(tick: Tick): void;
    tick(tick: Tick): TickResult;
}
export declare class RepeatUntilFailure extends Decorator {
    private _maxLoop;
    constructor(id: string, title: string, description: string, properties: any);
    open(tick: Tick): void;
    tick(tick: Tick): TickResult;
}
export declare class RepeatUntilSuccess extends Decorator {
    private _maxLoop;
    constructor(id: string, title: string, description: string, properties: any);
    open(tick: Tick): void;
    tick(tick: Tick): TickResult;
}
