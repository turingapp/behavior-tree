export declare enum TickResult {
    Failure = 0,
    Success = 1,
    Running = 2,
    Error = 3,
}
export declare enum NodeType {
    Composite = 0,
    Decorator = 1,
    Action = 2,
    Condition = 3,
}
