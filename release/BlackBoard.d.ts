export declare class Blackboard {
    private _treeMemory;
    constructor();
    private getTreeMemory(treeId);
    get(name: string, treeId: string, nodeId: string): any;
    set(name: string, value: any, treeId: string, nodeId: string): void;
}
