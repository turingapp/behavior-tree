import {BaseNode} from './BaseNode';
import {Composite} from './CoreNodes';
import {Tick} from './Tick';
import {TickResult} from './enums';

export class Sequence extends Composite {
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'Sequence', title, description, properties);
    }

    tick(tick: Tick): TickResult {
        for (let child of this.children) {
            let result = child.execute(tick);
            if (result != TickResult.Success) {
                return result;
            }
        }

        return TickResult.Success;
    }
}

export class MemSequence extends Composite {
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'MemSequence', title, description, properties);
    }

    open(tick: Tick): void {
        tick.blackboard.set('runningChild', 0, tick.tree.id, this.id);
    }

    tick(tick: Tick): TickResult {
        let childIdx = tick.blackboard.get('runningChild', tick.tree.id, this.id);
        for (let i = childIdx; i < this.children.length; i++) {
            let result = this.children[i].execute(tick);
            if (result != TickResult.Success) {
                if (result == TickResult.Running) {
                    tick.blackboard.set('runningChild', i, tick.tree.id, this.id);
                }
                return result;
            }
        }

        return TickResult.Success;
    }
}

export class Priority extends Composite {
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'Priority', title, description, properties);
    }

    tick(tick: Tick): TickResult {
        for (let child of this.children) {
            let result = child.execute(tick);
            if (result != TickResult.Failure) {
                return result;
            }
        }

        return TickResult.Failure;
    }
}

export class MemPriority extends Composite {
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'MemPriority', title, description, properties);
    }

    open(tick: Tick): void {
        tick.blackboard.set('runningChild', 0, tick.tree.id, this.id);
    }

    tick(tick: Tick): TickResult {
        let childIdx = tick.blackboard.get('runningChild', tick.tree.id, this.id);
        for (let i = childIdx; i < this.children.length; i++) {
            let result = this.children[i].execute(tick);
            if (result != TickResult.Failure) {
                if (result == TickResult.Running) {
                    tick.blackboard.set('runningChild', i, tick.tree.id, this.id);
                }
                return result;
            }
        }

        return TickResult.Failure;
    }
}