"use strict";
var TickResult;
(function (TickResult) {
    TickResult[TickResult["Failure"] = 0] = "Failure";
    TickResult[TickResult["Success"] = 1] = "Success";
    TickResult[TickResult["Running"] = 2] = "Running";
    TickResult[TickResult["Error"] = 3] = "Error";
})(TickResult = exports.TickResult || (exports.TickResult = {}));
var NodeType;
(function (NodeType) {
    NodeType[NodeType["Composite"] = 0] = "Composite";
    NodeType[NodeType["Decorator"] = 1] = "Decorator";
    NodeType[NodeType["Action"] = 2] = "Action";
    NodeType[NodeType["Condition"] = 3] = "Condition";
})(NodeType = exports.NodeType || (exports.NodeType = {}));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVudW1zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxJQUFZLFVBS1g7QUFMRCxXQUFZLFVBQVU7SUFDbEIsaURBQVcsQ0FBQTtJQUNYLGlEQUFPLENBQUE7SUFDUCxpREFBTyxDQUFBO0lBQ1AsNkNBQUssQ0FBQTtBQUNULENBQUMsRUFMVyxVQUFVLEdBQVYsa0JBQVUsS0FBVixrQkFBVSxRQUtyQjtBQUVELElBQVksUUFLWDtBQUxELFdBQVksUUFBUTtJQUNoQixpREFBUyxDQUFBO0lBQ1QsaURBQVMsQ0FBQTtJQUNULDJDQUFNLENBQUE7SUFDTixpREFBUyxDQUFBO0FBQ2IsQ0FBQyxFQUxXLFFBQVEsR0FBUixnQkFBUSxLQUFSLGdCQUFRLFFBS25CIiwiZmlsZSI6ImVudW1zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGVudW0gVGlja1Jlc3VsdCB7XG4gICAgRmFpbHVyZSA9IDAsXG4gICAgU3VjY2VzcyxcbiAgICBSdW5uaW5nLFxuICAgIEVycm9yXG59XG5cbmV4cG9ydCBlbnVtIE5vZGVUeXBlIHtcbiAgICBDb21wb3NpdGUsXG4gICAgRGVjb3JhdG9yLFxuICAgIEFjdGlvbixcbiAgICBDb25kaXRpb25cbn0iXX0=
