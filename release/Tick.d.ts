import { BehaviorTree } from './behaviortree';
import { BaseNode } from './basenode';
import { Blackboard } from './BlackBoard';
export declare class Tick {
    private _blackboard;
    private _tree;
    private _openedNodes;
    private _nodeCount;
    constructor(bb: Blackboard, t: BehaviorTree);
    readonly blackboard: Blackboard;
    readonly tree: BehaviorTree;
    readonly openedNodes: BaseNode[];
    readonly nodeCount: number;
    openNode(node: BaseNode): void;
    closeNode(node: BaseNode): void;
    enterNode(node: BaseNode): void;
    exitNode(node: BaseNode): void;
    tickNode(node: BaseNode): void;
}
