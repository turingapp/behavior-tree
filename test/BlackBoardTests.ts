import {expect} from 'chai';
import * as mocha from 'mocha';

import {Blackboard} from '../release/Blackboard';

describe('BlackBoard object', () => {
    it('should keep track of tree scoped information', () => {
        let bb: Blackboard = new Blackboard();

        bb.set('id', 1, 'tree1', null);
        bb.set('id', 2, 'tree2', null);

        expect(bb.get('id', 'tree1', null)).to.be.equal(1);
        expect(bb.get('id', 'tree2', null)).to.be.equal(2);
    });

    it('should keep track of node scoped information', () => {
        let bb: Blackboard = new Blackboard();

        bb.set('id', 1, 'tree1', 'node1');
        bb.set('id', 2, 'tree1', 'node2');

        expect(bb.get('id', 'tree1', 'node1')).to.be.equal(1);
        expect(bb.get('id', 'tree1', 'node2')).to.be.equal(2);
    });
});