import {expect} from 'chai';
import * as mocha from 'mocha';

import {Tick} from '../release/Tick';
import {Blackboard} from '../release/BlackBoard';
import {BehaviorTree} from '../release/BehaviorTree';
import {BaseNode} from '../release/BaseNode';
import {NodeType, TickResult} from '../release/enums';
import {Condition, Action} from '../release/CoreNodes';
import {Priority, MemSequence} from '../release/Composite';
import {Repeater} from '../release/Repeater';

describe('Behavior Tree', () => {
    it('should load the JSON properly', () => {
        let data: any = {
            'title'       : 'A JSON Behavior Tree',
            'description' : 'This description',
            'root'        : '1',
            'properties'  : {
                'variable' : 'value'
            },
            'nodes' : {
                // Test properties and children
                '1': {
                    'id'          : '1',
                    'name'        : 'Priority',
                    'title'       : 'Root Node',
                    'description' : 'Root Description',
                    'children'    : ['2', '3'],
                    'properties'  : {
                        'var1' : 123,
                        'composite': {
                            'var2' : true,
                            'var3' : 'value'
                        }
                    }
                },
                // Test child
                '2': {
                    'name'        : 'Repeater',
                    'title'       : 'Node 1',
                    'description' : 'Node 1 Description',
                    'child'       : '4',
                },
                '3': {
                    'name'        : 'MemSequence',
                    'title'       : 'Node 2',
                    'description' : 'Node 2 Description',
                    'children'    : [],
                },
                // Test parameters
                '4': {
                    'name'        : 'Condition',
                    'title'       : 'Node 3',
                    'description' : 'Node 3 Description',
                    'child'       : null,
                    'properties'  : {
                        'someCondition' : 1 // works as constructor argument now
                    }
                }
            }
        };
        let bt: BehaviorTree = new BehaviorTree(null, null, null, null);
        bt.load(data, new Map());

        expect(bt.root.category).to.be.equal(NodeType.Composite);
        expect(bt.root instanceof Priority).to.be.true;

        let p: Priority = <Priority>bt.root;
        expect(p.children).to.be.of.length(2);
        expect(p.children[0].category).to.be.equal(NodeType.Decorator);
        expect(p.children[0] instanceof Repeater).to.be.true;
        expect(p.children[1].category).to.be.equal(NodeType.Composite);
        expect(p.children[1] instanceof MemSequence).to.be.true;

        let r: Repeater = <Repeater>p.children[0];
        expect(r.child).to.be.not.null;
        expect(r.child instanceof Condition).to.be.true;

        let c: Condition = <Condition>r.child;
        expect(c.properties['someCondition']).to.be.equal(1);

        let ms: MemSequence = <MemSequence>p.children[1];
        expect(ms.children).to.be.empty;
    });

    it('should load custom nodes', () => {
        let data: any = {
            'id'          : 'treeId',
            'root'        : '1',
            'nodes' : {
                '1': {
                    'id'          : '1',
                    'name'        : 'Priority',
                    'children'    : ['2', '3'],
                },
                '2': {
                    'id'          : '2',   
                    'name'        : 'Introduce'
                },
                '3': {
                    'id'          : '3',
                    'name'        : 'MemSequence',
                    'children'    : ['4', '5'],
                },
                '4': {
                    'id'          : '4',
                    'name'        : 'CheckCondition',
                },
                '5': {
                    'id'          : '5',
                    'name'        : 'AskUser'
                }
            }
        };
        
        class Introduce extends Action {
            introductionCount: number = 0;

            tick(tick: Tick): TickResult {
                if (!tick.blackboard.get('introduced', tick.tree.id, null)) {
                    this.introductionCount++;
                    return TickResult.Success;
                } else {
                    return TickResult.Failure;
                }
            }
        };

        class CheckCondition extends Condition {
            conditionMet: boolean = false;
            tick(tick: Tick): TickResult {
                if (!tick.blackboard.get('conditionMet', tick.tree.id, null)) {
                    return TickResult.Running;
                } else {
                    this.conditionMet = true;
                    return TickResult.Success;
                }
            }
        }

        class AskUser extends Action {
            askUserCalled: number = 0;

            tick(tick: Tick): TickResult {
                this.askUserCalled++;
                return TickResult.Success;
            }
        }

        let bb: Blackboard = new Blackboard();
        let ctorMap: Map<string, typeof BaseNode> = new Map();
        ctorMap.set('Introduce', Introduce);
        ctorMap.set('CheckCondition', CheckCondition);
        ctorMap.set('AskUser', AskUser);

        let bt: BehaviorTree = new BehaviorTree(null, null, null, null);
        bt.load(data, ctorMap);

        var i: Introduce = <Introduce>(<Priority>bt.root).children[0];
        var ms: MemSequence = <MemSequence>(<Priority>bt.root).children[1];
        var cc: CheckCondition = <CheckCondition>ms.children[0];
        var au: AskUser = <AskUser>ms.children[1];

        bt.tick(bb);
        expect(i.introductionCount).to.be.equal(1);
        expect(cc.conditionMet).to.be.false;
        expect(au.askUserCalled).to.be.equal(0);
        bt.tick(bb);
        expect(i.introductionCount).to.be.equal(2);        
        expect(cc.conditionMet).to.be.false;
        expect(au.askUserCalled).to.be.equal(0);
        
        bb.set('introduced', true, bt.id, null);
        bt.tick(bb);
        expect(i.introductionCount).to.be.equal(2);        
        expect(cc.conditionMet).to.be.false;
        expect(au.askUserCalled).to.be.equal(0);

        bb.set('conditionMet', true, bt.id, null);
        bt.tick(bb);
        expect(i.introductionCount).to.be.equal(2);        
        expect(cc.conditionMet).to.be.true;
        expect(au.askUserCalled).to.be.equal(1);
    })
});
