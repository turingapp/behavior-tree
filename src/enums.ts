export enum TickResult {
    Failure = 0,
    Success,
    Running,
    Error
}

export enum NodeType {
    Composite,
    Decorator,
    Action,
    Condition
}