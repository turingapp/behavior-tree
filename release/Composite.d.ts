import { Composite } from './CoreNodes';
import { Tick } from './Tick';
import { TickResult } from './enums';
export declare class Sequence extends Composite {
    constructor(id: string, title: string, description: string, properties: any);
    tick(tick: Tick): TickResult;
}
export declare class MemSequence extends Composite {
    constructor(id: string, title: string, description: string, properties: any);
    open(tick: Tick): void;
    tick(tick: Tick): TickResult;
}
export declare class Priority extends Composite {
    constructor(id: string, title: string, description: string, properties: any);
    tick(tick: Tick): TickResult;
}
export declare class MemPriority extends Composite {
    constructor(id: string, title: string, description: string, properties: any);
    open(tick: Tick): void;
    tick(tick: Tick): TickResult;
}
