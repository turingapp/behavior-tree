import {BehaviorTree} from './behaviortree';
import {BaseNode} from './basenode';
import {Blackboard} from './BlackBoard';

export class Tick {
    private _blackboard: Blackboard;
    private _tree: BehaviorTree;
    private _openedNodes: BaseNode[];
    private _nodeCount: number;

    constructor(bb: Blackboard, t: BehaviorTree) {
        this._blackboard = bb;
        this._tree = t;
        this._openedNodes = [];
        this._nodeCount = 0;
    }
    
    get blackboard() : Blackboard {
        return this._blackboard;
    }

    get tree(): BehaviorTree {
        return this._tree;
    } 

    get openedNodes(): BaseNode[] {
        return this._openedNodes;
    }

    get nodeCount(): number {
        return this._nodeCount;
    }

    openNode(node: BaseNode): void {

    }

    closeNode(node: BaseNode): void {
        this._openedNodes.pop();
    }

    enterNode(node: BaseNode): void {
        this._nodeCount++;
        this._openedNodes.push(node);
    }

    exitNode(node: BaseNode): void {

    }

    tickNode(node: BaseNode): void {

    }
}