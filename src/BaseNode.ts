import {NodeType, TickResult} from './enums';
import {Tick} from './tick';
import {Blackboard} from './BlackBoard';

export class BaseNode {
    protected _id: string;
    protected _name: string;
    protected _title: string;
    protected _description: string;
    protected _properties: any;

    protected _category: NodeType;

    constructor(id: string, name: string, title: string, description: string, properties: any) {
        this._id = id;
        this._name = name;
        this._title = title;
        this._description = description;
        this._properties = properties;
    }
    
    get id(): string {
        return this._id;
    }
    
    get category(): NodeType {
        return this._category;
    }

    get properties(): any {
        return this._properties;
    }

    execute(tick: Tick) {
        this.enter(tick);
        if (!tick.blackboard.get('isOpen', tick.tree.id, this.id))
            this.open(tick);
        
        var result = this.tick(tick);

        if (result != TickResult.Running)
            this.close(tick);

        this.exit(tick);

        return result;
    }

    enter(tick: Tick): void {
        tick.enterNode(this);
    }

    exit(tick: Tick): void {
        tick.exitNode(this);
    }

    open(tick: Tick): void {
        tick.openNode(this);
        tick.blackboard.set('isOpen', true, tick.tree.id, this.id);
    }

    close(tick: Tick): void {
        tick.closeNode(this);
        tick.blackboard.set('isOpen', false, tick.tree.id, this.id);
    }

    tick(tick: Tick): TickResult {
        tick.tickNode(this);
        throw new Error('NotImplemented');
    }
}