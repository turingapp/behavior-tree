import {BaseNode} from './BaseNode';
import {Tick} from './Tick';
import {TickResult, NodeType} from './enums';
import {Blackboard} from './BlackBoard';
import {Composite, Decorator, Action, Condition} from './CoreNodes';
import {Repeater, RepeatUntilFailure, RepeatUntilSuccess} from './Repeater';
import {Sequence, Priority, MemSequence, MemPriority} from './Composite';

export class BehaviorTree {
    private _id: string;
    private _title: string;
    private _description: string;
    private _properties: any;
    private _root: BaseNode;

    private static coreCtors: { [name: string]: typeof BaseNode } = {
        'Composite': Composite,
        'Decorator': Decorator,
        'Action': Action,
        'Condition': Condition,
        'Sequence': Sequence,
        'Priority': Priority,
        'MemSequence': MemSequence,
        'MemPriority': MemPriority,
        'Repeater': Repeater,
        'RepeatUntilFailure': RepeatUntilFailure,
        'RepeatUntilSuccess': RepeatUntilSuccess
    }

    constructor(id: string, title: string, description: string, properties: any) {
        this._id = id;
        this._title = title;
        this._description = description;
        this._properties = properties;
    }

    get id(): string { 
        return this._id;
    }

    get title(): string {
        return this._title;
    }

    get description(): string {
        return this._description;
    }

    get properties(): any {
        return this._properties;
    }

    get root(): BaseNode {
        return this._root;
    }

    set root(node: BaseNode) {
        this._root = node;
    }

    load(data: any, ctors: Map<string, typeof BaseNode>) {
        this._title = data.title || this._title;
        this._description = data.description || this._description;
        this._properties = data.properties || this._properties;

        let nodes: {[id: string]: BaseNode} = {};
        for (let id in data.nodes) {
            let spec = data.nodes[id];
            let node: BaseNode;

            if (ctors.has(spec.name)) {
                let ctor = ctors.get(spec.name);
                node = new ctor(spec.id, spec.name, spec.title, spec.description, spec.properties);
            } else if (BehaviorTree.coreCtors[spec.name]) {
                let ctor = BehaviorTree.coreCtors[spec.name];
                node = new ctor(spec.id, spec.name, spec.title, spec.description, spec.properties);
            } else {
                throw new Error('Unknown node type');
            }

            nodes[id] = node;
        }

        for (let id in data.nodes) {
            let spec = data.nodes[id];
            let node = nodes[id];

            if (node.category == NodeType.Composite && spec.children) {
                for (let i = 0; i < spec.children.length; i++) {
                    let childId = spec.children[i];
                    (<Composite>node).children.push(nodes[childId]);
                }
            } else if (node.category == NodeType.Decorator && spec.child) {
                (<Decorator>node).child = nodes[spec.child];
            }
        }

        this._root = nodes[data.root];
    }

    tick(blackboard: Blackboard): TickResult {
        let tick = new Tick(blackboard, this);

        let result = this.root.execute(tick);

        let lastOpenedNodes: BaseNode[] = blackboard.get('openedNodes', this.id, null) || [];
        let currOpenedNodes = tick.openedNodes.slice(0);

        let start = 0;
        for (let i = 0; i < Math.min(lastOpenedNodes.length, currOpenedNodes.length); i++) {
            start = i;
            if (currOpenedNodes[i] !== lastOpenedNodes[i])
                break;
        }

        for (let i = lastOpenedNodes.length - 1; i > start; i--) {
            lastOpenedNodes[i].close(tick);
        }

        blackboard.set('openedNodes', currOpenedNodes, this.id, null);
        blackboard.set('nodeCount', tick.nodeCount, this.id, null);
        return result;
    }
}