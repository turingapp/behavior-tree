import {BaseNode} from './BaseNode';
import {Decorator} from './CoreNodes';
import {Tick} from './Tick';
import {TickResult} from './enums';

export class Repeater extends Decorator {
    private _maxLoop: number = -1;
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'Repeater', title, description, properties);
    }

    open(tick: Tick): void {
        tick.blackboard.set('i', 0, tick.tree.id, this.id);
    }

    tick(tick: Tick): TickResult {
        if (!this.child) {
            return TickResult.Error;
        }

        let i: number = tick.blackboard.get('i', tick.tree.id, this.id);
        let result = TickResult.Success;

        while (this._maxLoop < 0 || i < this._maxLoop) {
            result = this.child.execute(tick);

            if (result == TickResult.Success || result == TickResult.Failure)
                i++;
            else 
                break;
        }

        tick.blackboard.set('i', i, tick.tree.id, this.id);
        return result;
    }
}

export class RepeatUntilFailure extends Decorator {
    private _maxLoop: number = -1;
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'RepeatUntilFailure', title, description, properties);
    }

    open(tick: Tick): void {
        tick.blackboard.set('i', 0, tick.tree.id, this.id);
    }

    tick(tick: Tick): TickResult {
        if (!this.child) {
            return TickResult.Error;
        }

        let i: number = tick.blackboard.get('i', tick.tree.id, this.id);
        let result = TickResult.Success;

        while (this._maxLoop < 0 || i < this._maxLoop) {
            result = this.child.execute(tick);

            if (result == TickResult.Success)
                i++;
            else 
                break;
        }

        tick.blackboard.set('i', i, tick.tree.id, this.id);
        return result;
    }
}

export class RepeatUntilSuccess extends Decorator {
    private _maxLoop: number = -1;
    constructor(id: string, title: string, description: string, properties: any) {
        super(id, 'RepeatUntilSuccess', title, description, properties);
    }

    open(tick: Tick): void {
        tick.blackboard.set('i', 0, tick.tree.id, this.id);
    }

    tick(tick: Tick): TickResult {
        if (!this.child) {
            return TickResult.Error;
        }

        let i: number = tick.blackboard.get('i', tick.tree.id, this.id);
        let result = TickResult.Success;

        while (this._maxLoop < 0 || i < this._maxLoop) {
            result = this.child.execute(tick);

            if (result == TickResult.Failure)
                i++;
            else 
                break;
        }

        tick.blackboard.set('i', i, tick.tree.id, this.id);
        return result;
    }
}