import {BaseNode} from './BaseNode';

interface TreeMemory {
    baseMemory: Map<string, any>;
    nodeMemory: Map<string, Map<string, any>>;
    openNodes: BaseNode[];
}

export class Blackboard {
    private _treeMemory: Map<string, TreeMemory>;

    constructor() {
        this._treeMemory = new Map();
    }

    private getTreeMemory(treeId: string): TreeMemory {
        if (!this._treeMemory.has(treeId)) {
            this._treeMemory.set(treeId, {
                baseMemory: new Map(),
                nodeMemory: new Map(),
                openNodes: []
            });
        }

        return this._treeMemory.get(treeId);
    }

    get(name: string, treeId: string, nodeId: string): any {
        let treeMemory = this.getTreeMemory(treeId);
        if (nodeId) {
            if (treeMemory.nodeMemory.has(nodeId))
                return treeMemory.nodeMemory.get(nodeId).get(name);
        } else {
            return treeMemory.baseMemory.get(name);
        }

        return null;
    }

    set(name: string, value: any, treeId: string, nodeId: string): void {
        let treeMemory = this.getTreeMemory(treeId);
        if (nodeId) {
            if (!treeMemory.nodeMemory.has(nodeId)) 
                treeMemory.nodeMemory.set(nodeId, new Map());
            treeMemory.nodeMemory.get(nodeId).set(name, value);
        } else {
            treeMemory.baseMemory.set(name, value);
        }
    }
}