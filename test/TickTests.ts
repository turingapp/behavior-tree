import {expect} from 'chai';
import * as mocha from 'mocha';

import {Tick} from '../release/Tick';
import {BaseNode} from '../release/BaseNode';

describe('Tick object', () => {
    it('should initialize', () => {
        let t: Tick = new Tick(null, null);
        expect(t.blackboard).to.be.equal(null);
        expect(t.nodeCount).to.be.equal(0);
        expect(t.openedNodes).to.be.empty;
    });

    it('should update tick on enter', () => {
        let t: Tick = new Tick(null, null);
        let n: BaseNode = new BaseNode('id1', 'CustomNode', 'Custom Node', '', {});

        t.enterNode(n);
        expect(t.nodeCount).to.be.equal(1);
        expect(t.openedNodes).to.include(n);
    });

    it('should update tick on close', () => {
        let t: Tick = new Tick(null, null);
        let n: BaseNode = new BaseNode('id1', 'CustomNode', 'Custom Node', '', {});

        t.enterNode(n);
        t.closeNode(n);
        expect(t.nodeCount).to.be.equal(1);
        expect(t.openedNodes).to.be.empty;        
    });
});