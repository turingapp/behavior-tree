import { NodeType, TickResult } from './enums';
import { Tick } from './tick';
export declare class BaseNode {
    protected _id: string;
    protected _name: string;
    protected _title: string;
    protected _description: string;
    protected _properties: any;
    protected _category: NodeType;
    constructor(id: string, name: string, title: string, description: string, properties: any);
    readonly id: string;
    readonly category: NodeType;
    readonly properties: any;
    execute(tick: Tick): TickResult;
    enter(tick: Tick): void;
    exit(tick: Tick): void;
    open(tick: Tick): void;
    close(tick: Tick): void;
    tick(tick: Tick): TickResult;
}
