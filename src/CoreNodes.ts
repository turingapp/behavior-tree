import {BaseNode} from './BaseNode';
import {TickResult, NodeType} from './enums';

export class Action extends BaseNode {
    constructor(id: string, name: string, title: string, description: string, properties: any) {
        super(id, name, title, description, properties);
        this._category = NodeType.Action;
    }
}

export class Composite extends BaseNode {
    private _children: BaseNode[];

    constructor(id: string, name: string, title: string, description: string, properties: any) {
        super(id, name, title, description, properties);
        this._category = NodeType.Composite;
        this._children = [];
    }

    get children(): BaseNode[] {
        return this._children;
    }
}

export class Decorator extends BaseNode {
    private _child: BaseNode;

    constructor(id: string, name: string, title: string, description: string, properties: any) {
        super(id, name, title, description, properties);
        this._category = NodeType.Decorator;
    }    

    get child(): BaseNode {
        return this._child;
    }

    set child(node: BaseNode) {
        this._child = node;
    }
}

export class Condition extends BaseNode {
    constructor(id: string, name: string, title: string, description: string, properties: any) {
        super(id, name, title, description, properties);
        this._category = NodeType.Condition;
    }    
}