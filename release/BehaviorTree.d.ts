import { BaseNode } from './BaseNode';
import { TickResult } from './enums';
import { Blackboard } from './BlackBoard';
export declare class BehaviorTree {
    private _id;
    private _title;
    private _description;
    private _properties;
    private _root;
    private static coreCtors;
    constructor(id: string, title: string, description: string, properties: any);
    readonly id: string;
    readonly title: string;
    readonly description: string;
    readonly properties: any;
    root: BaseNode;
    load(data: any, ctors: Map<string, typeof BaseNode>): void;
    tick(blackboard: Blackboard): TickResult;
}
